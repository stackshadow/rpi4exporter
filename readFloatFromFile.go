package main

import (
	"io/ioutil"
	"strconv"
	"strings"
)

func readFloatFromFile(filename string) (float64, error) {

	// read the value
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return 0, err
	}

	contentString := strings.Trim(string(content), "\n")

	// convert it to float
	value, err := strconv.ParseFloat(string(contentString), 64)
	if err != nil {
		return 0, err
	}

	return value, nil
}
