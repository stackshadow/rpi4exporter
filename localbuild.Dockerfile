############################################## SCRATCH ##############################################
FROM scratch

ENV LD_LIBRARY_PATH /usr/local/lib:/usr/lib:/lib:/usr/lib64:/lib64

# copy new root
COPY newroot/ /


USER 1000
CMD ["/usr/bin/exporter"]


