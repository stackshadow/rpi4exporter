

build:
	mkdir -p newroot/{etc,bin,lib,lib64,run,tmp}
	mkdir -p newroot/usr/{bin,lib,lib64}
	cp /usr/lib/ld-*.so* /usr/lib/libpthread*.so* /usr/lib/libc.*so* /usr/lib/libc-*.so* newroot/lib
	cp rpi4exporter newroot/usr/bin/exporter
	# cp /usr/bin/ldd newroot/usr/bin/ldd
	docker build --no-cache -f localbuild.Dockerfile .
	rm -fR newroot
