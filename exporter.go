package main

import (
	"log"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	defaultRegistry                         = prometheus.NewRegistry()
	defaultRegisterer prometheus.Registerer = defaultRegistry
	defaultGatherer   prometheus.Gatherer   = defaultRegistry
)

func main() {

	logrus.SetLevel(logrus.DebugLevel)

	// register metrics
	filesystemRegister(defaultRegisterer)
	networkRegister(defaultRegisterer)
	tempRegister(defaultRegisterer)

	// 1 minute-slot
	go func() {
		seconds := time.Duration(60/2) * time.Second
		tempRefresh()
		networkRefresh()

		for {
			time.Sleep(seconds)
			tempRefresh()

			time.Sleep(seconds)
			networkRefresh()
		}
	}()

	// 10 minute-slot
	go func() {
		seconds := time.Duration(600/1) * time.Second
		filesystemRefresh()

		for {
			time.Sleep(seconds)
			filesystemRefresh()
		}
	}()

	// setup http-handler
	emptyHandler := promhttp.InstrumentMetricHandler(
		defaultRegisterer, promhttp.HandlerFor(defaultGatherer, promhttp.HandlerOpts{}),
	)

	http.Handle("/metrics", emptyHandler)
	log.Fatal(http.ListenAndServe(":3333", nil))
}
