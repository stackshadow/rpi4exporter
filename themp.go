package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
)

var (
	thermal = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "thermal",
			Help: "Thermal infos",
		},
		[]string{"class"},
	)
)

func tempRegister(reg prometheus.Registerer) {
	defaultRegisterer.MustRegister(thermal)
}

func tempRefresh() {

	// read temp
	temp, err := readFloatFromFile("/sys/class/thermal/thermal_zone0/temp")
	if err != nil {
		logrus.Warn(err)
	}

	// write it to metric
	thermal.With(prometheus.Labels{"class": "zone0"}).Set(temp)

	logrus.
		WithField("class", "zone0").
		WithField("thermal", temp).
		Debug()

}
