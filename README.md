# What

This is a small raspberry-pi-4 metrics-exporter for prometheus written in go.

It provide the following metrics:
- temperatur of thermal zone 0
- used and maximum bytes of normal filesystems
- tx/rx bytes of eth0

# Why

The "default" exporter don't export the current temperature of an raspberry-pi-4

# Build on a raspberry pi
- `env GOOS=linux GOARCH=arm GOARM=7 go build -o rpi4exporter`
- `make`
