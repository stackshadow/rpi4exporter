package main

import (
	"bufio"
	"io"
	"os"
	"strings"
	"syscall"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
	"golang.org/x/sys/unix"
)

var (
	nodeFilesystemSizeBytes = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "node_filesystem_size_bytes",
			Help: "Size of filesystem in bytes",
		},
		[]string{"device"},
	)
	nodeFilesystemAvailBytes = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "node_filesystem_avail_bytes",
			Help: "Aavailable bytes",
		},
		[]string{"device"},
	)
)

func filesystemRegister(reg prometheus.Registerer) {
	defaultRegisterer.MustRegister(nodeFilesystemSizeBytes)
	defaultRegisterer.MustRegister(nodeFilesystemAvailBytes)

	// read all available filesystems
	var err error
	mountedFilesystems, err = MountsNew()
	if err != nil {
		logrus.Error(err)
		return
	}

	mountedFilesystems.RemoveFs([]string{
		"autofs",
		"binfmt_misc",
		"bpf",
		"cgroup",
		"cgroup2",
		"configfs",
		"debugfs",
		"devpts",
		"devtmpfs",
		"mqueue",
		"nsfs",
		"overlay",
		"proc",
		"securityfs",
		"sysfs",
		"tmpfs",
	})
	mountedFilesystems.UniqueSource()
}

func filesystemRefresh() {

	for _, curMount := range mountedFilesystems.array {

		buf := new(unix.Statfs_t)
		err := unix.Statfs(curMount.Target, buf)
		if err != nil {
			logrus.Warn(err)
			return
		}

		nodeFilesystemSizeBytes.With(
			prometheus.Labels{
				"device": curMount.Source,
			},
		).Set(float64(buf.Blocks) * float64(buf.Bsize))
		logrus.
			WithField("device", curMount.Source).
			WithField("node_filesystem_size_bytes", float64(buf.Blocks)*float64(buf.Bsize)).
			Debug()

		nodeFilesystemAvailBytes.With(
			prometheus.Labels{
				"device": curMount.Source,
			},
		).Set(float64(buf.Bavail) * float64(buf.Bsize))
		logrus.
			WithField("device", curMount.Source).
			WithField("node_filesystem_avail_bytes", float64(buf.Bavail)*float64(buf.Bsize)).
			Debug()

	}
}

type mount struct {
	Source  string
	Target  string
	Type    string
	options string
}

type mounts struct {
	array []mount
}

var mountedFilesystems mounts

func MountsNew() (mounts, error) {

	var newMounts mounts

	// open mounts
	file, err := os.Open("/proc/self/mounts")
	if err != nil {
		return newMounts, err
	}
	defer file.Close()

	// read all lines
	reader := bufio.NewReaderSize(file, 64*1024)
	for {

		// read single line
		line, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				return newMounts, nil
			}
			return newMounts, err
		}

		// parse the line
		parts := strings.SplitN(string(line), " ", 5)
		if len(parts) != 5 {
			return newMounts, syscall.EIO
		}

		newMount := mount{parts[0], parts[1], parts[2], parts[3]}
		newMounts.array = append(newMounts.array, newMount)
	}

	return newMounts, nil
}

func (cur *mounts) RemoveFs(ignorefs []string) {

	var newMounts []mount

	for _, curMount := range cur.array {

		// check if the mountpoint should be ignored over fs-type
		ignore := false
		for _, ignoredFS := range ignorefs {
			if ignoredFS == curMount.Type {
				ignore = true
			}
		}
		if ignore == true {
			continue
		}

		newMounts = append(newMounts, curMount)
	}

	cur.array = newMounts
}

func (cur *mounts) UniqueSource() {

	// we create a map of mounts
	mountMap := make(map[string]mount)

	// fill the map
	for _, curMount := range cur.array {
		mountMap[curMount.Source] = curMount
	}

	// create an array
	var mountArray []mount
	for _, curMount := range mountMap {
		mountArray = append(mountArray, curMount)
	}

	// save it
	cur.array = mountArray
}
