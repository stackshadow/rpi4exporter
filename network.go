package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
)

var (
	networkRxBytes = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "network_rx_bytes",
			Help: "Received bytes",
		},
		[]string{"interface"},
	)
	networkTxBytes = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "network_tx_bytes",
			Help: "Transmited bytes",
		},
		[]string{"interface"},
	)
)

func networkRegister(reg prometheus.Registerer) {
	defaultRegisterer.MustRegister(networkRxBytes)
	defaultRegisterer.MustRegister(networkTxBytes)
}

func networkRefresh() {
	// read
	rx, err := readFloatFromFile("/sys/class/net/eth0/statistics/rx_bytes")
	if err != nil {
		logrus.Warn(err)
	}
	tx, err := readFloatFromFile("/sys/class/net/eth0/statistics/tx_bytes")
	if err != nil {
		logrus.Warn(err)
	}

	// write it to metric
	networkRxBytes.With(prometheus.Labels{"interface": "eth0"}).Set(rx)
	logrus.
		WithField("interface", "eth0").
		WithField("network_rx_bytes", rx).
		Debug()

	networkTxBytes.With(prometheus.Labels{"interface": "eth0"}).Set(tx)
	logrus.
		WithField("interface", "eth0").
		WithField("network_tx_bytes", tx).
		Debug()

}
